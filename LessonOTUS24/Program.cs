﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LessonOTUS24
{
    class Program
    {
        private static long[] list, list2, list3;
        private static long sum1 = 0, sum2 = 0, sumAll = 0;
        private static byte count = 0;
        private static Stopwatch watcher2 = new Stopwatch();

        static void Main(string[] args)
        {
            list = new long[100_000];
            int i = 1;
            do
            {
                list[i] = i;
                i++;
            } while (i < 100_000);

            ForeachSum(list);
            ForeachSumThread();
            ParralelSum();

            Console.WriteLine("Hello World!");
        }

        private static void ForeachSum(long[] list)
        {
            long sum = 0;
            var watcher = Stopwatch.StartNew();
            foreach (var i in list)
                sum += i;
            watcher.Stop();
            Console.WriteLine("ForeachSum time: " + watcher.ElapsedMilliseconds);
        }

        private static void ThreadOne()
        {
            foreach (var i in list2)
                sum1 += i;
            count++;
            if (count == 2)
            {
                sumAll = sum1 + sum2;
                watcher2.Stop();
            }
        }

        private static void ThreadTwo()
        {
            foreach (var i in list3)
                sum2 += i;
            count++;
            if (count == 2)
            {
                sumAll = sum1 + sum2;
                watcher2.Stop();
            }
        }

        private static void ForeachSumThread()
        {
            list3 = list.Skip(list.Length / 2).ToArray();
            list2 = list.Take(list.Length / 2).ToArray();
            Thread thread1 = new Thread(ThreadOne);
            Thread thread2 = new Thread(ThreadTwo);
            thread1.Start();
            thread2.Start();
            watcher2 = Stopwatch.StartNew();
            Task.Delay(1000).GetAwaiter().GetResult();
            Console.WriteLine("SumThread time: " + watcher2.ElapsedMilliseconds);
        }

        private static void ParralelSum()
        {
            var watch = new Stopwatch();
            watch.Start();
            sumAll += list.AsParallel().Sum();
            watch.Stop();
            Console.WriteLine("ParralelSum time: " + watch.ElapsedMilliseconds);
        }
    }
}
